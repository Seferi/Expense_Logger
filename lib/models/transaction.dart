import 'package:flutter/foundation.dart';

class Transaction {
  final String id;
  final String title;
  final double amount;
  final DateTime date;

  Transaction({
    @required this.id,                  //Because these properties are required, we add the @required annotation and for that we import foundation.dart from flutter package
    @required this.title,
    @required this.amount,
    @required this.date
  });               //Named properties
}